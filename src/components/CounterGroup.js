import Counter from "./Counter";


const CounterGroup = ({ counts, onIncrement, onDecrement }) => {
    return (
        <div>
            {counts.map((count, index) => (
                <Counter
                    key={index}
                    value={count}
                    onIncrement={() => onIncrement(index)}
                    onDecrement={() => onDecrement(index)}
                />
            ))}
        </div>
    );
};

export default CounterGroup;