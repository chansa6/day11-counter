import { useState } from "react"; 
import CounterGroup from "./CounterGroup";
import CounterGroupSum from "./CounterGroupSum";
import CounterSizeGenerator from "./CounterSizeGenerator";

function MultipleCounter() {
    
    const [size, setSize] = useState(0);
    const [sum, setSum] = useState(0);
    const [counts, setCounts] = useState(Array(size).fill(0));


    const handleIncrement = (index) => {
        const updatedCounters = [...counts];
        updatedCounters[index]++;
        setCounts(updatedCounters);
        setSum(sum + 1);
    };

    const handleDecrement = (index) => {
        const updatedCounters = [...counts];
        updatedCounters[index]--;
        setCounts(updatedCounters);
        setSum(sum - 1);
    };

    const handleChangeSize = (newSize) => {
        setSize(newSize);
        setCounts(new Array(parseInt(newSize)).fill(0));
        setSum(0);
    };

    return (
        <div>
            <CounterSizeGenerator onSizeChange={handleChangeSize} />
            <CounterGroupSum sum={sum} /><br></br>
            <CounterGroup counts={counts} onIncrement={handleIncrement} onDecrement={handleDecrement} />
        </div>
    );
}

export default MultipleCounter;