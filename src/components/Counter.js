const Counter = ({ value, onIncrement, onDecrement }) => {
    return (
        <div>
            <button onClick={onIncrement}>+</button>
            <span className="counter-value">{value}</span>
            <button onClick={onDecrement}>-</button>
        </div>
    );
};

export default Counter;