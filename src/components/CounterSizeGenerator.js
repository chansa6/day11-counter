function CounterSizeGenerator({ onSizeChange }) {
    return (
        <div className="counter-size-container">
            Size: <input onChange={
                (event) => onSizeChange(event.target.value)
            } 
            type="number" min="0" />
        </div>
    )
}

export default CounterSizeGenerator;